export interface ListInterface {
  index: number;
  name: string;
  price_cents: number;
}
