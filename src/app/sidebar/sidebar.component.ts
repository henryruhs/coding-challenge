import { Component, OnInit } from '@angular/core';
import { InvoiceStore } from '../shared/invoice.store';
import { ListInterface } from './sidebar.interface';
import { TranslateService } from '@ngx-translate/core';
import { InvoiceInterface } from '../shared/invoice.interface';
import { calcItemPrice, calcInvoicePrice } from '../shared/invoice.helper';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  invoices: InvoiceInterface[];

  constructor(
    protected translateService: TranslateService,
    protected invoiceStore: InvoiceStore
  ) {}

  ngOnInit(): void {
    this.invoiceStore
      .getInstance()
      .subscribe(store => {
        this.invoices = store.invoices;
      });
  }

  handleImport(): void {
    this.translateService
      .get('import_message')
      .subscribe(translation => {
        const invoices: InvoiceInterface[] = JSON.parse(window.prompt(translation));

        if (invoices.length > 0) {
          this.invoiceStore.setStoreValue({
            current: 0,
            invoices
          });
        }
      });
  }

  handleExport(): void {
    window.alert(JSON.stringify(this.invoiceStore.getStoreValue().invoices, null, 2));
  }

  addInvoice(): void {
    this.invoiceStore.addInvoice();
  }

  removeInvoiceByIndex(index): void {
    this.invoiceStore.removeInvoiceByIndex(index);
  }

  getCurrent(): number {
    return this.invoiceStore.getCurrent();
  }

  setCurrent(index): void {
    this.invoiceStore.setCurrent(index);
  }

  getNettoCents(): number {
    return calcInvoicePrice(this.invoices);
  }

  protected mapInvoicesToList(invoices: InvoiceInterface[]): ListInterface[] {
    if (invoices.length > 0) {
      return invoices.map((value, index) => {
        return {
          index,
          name: value.customer_name,
          price_cents: calcItemPrice(value.line_items)
        };
      });
    }
    return [];
  }
}
