import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ContentComponent } from './content.component';

describe('ContentComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ContentComponent
      ],
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  });

  it('should create the component', () => {
    const fixture = TestBed.createComponent(ContentComponent);
    const component = fixture.componentInstance;

    expect(component).toBeTruthy();
  });
});
