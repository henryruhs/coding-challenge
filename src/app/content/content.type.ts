export type ControlType = 'checkbox' | 'number' | 'text';
