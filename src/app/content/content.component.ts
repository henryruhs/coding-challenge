import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { InvoiceStore } from '../shared/invoice.store';
import { ControlType } from './content.type';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  form: FormGroup;

  customerControls: string[] = [
    'customer_id',
    'customer_name',
    'customer_contact_person',
    'customer_address',
    'customer_zip',
    'customer_city'
  ];
  paymentControls: string[] = [
    'iban',
    'bic',
    'account_owner',
    'mandate_reference',
    'mandate_city',
    'mandate_date',
    'mandate_signee'
  ];
  invoiceOneControls: string[] = [
    'invoice_number',
    'invoice_period'
  ];
  invoiceTwoControls: string[] = [
    'invoice_date',
    'invoice_due_date'
  ];

  constructor(protected invoiceStore: InvoiceStore) {}

  ngOnInit(): void {
    this.invoiceStore
      .getInstance()
      .subscribe(store => {
        if (store.invoices.length > 0) {
          this.form = this.createFormGroup(this.invoiceStore.getInvoiceByIndex(this.invoiceStore.getCurrent()));
          this.form.valueChanges.subscribe(values => this.handleUpdate(values));
        }
      });
  }

  hasControl(control: string): boolean {
    return this.form.contains(control);
  }

  resolveControlType(control: string): ControlType {
    const value: ControlType = this.form?.get(control).value;

    if (typeof value === 'boolean') {
      return 'checkbox';
    }
    if (typeof value === 'number') {
      return 'number';
    }
    return 'text';
  }

  handleUpdate(values): void {
    this.invoiceStore.setInvoiceByIndex(this.invoiceStore.getCurrent(), values);
  }

  protected createFormGroup(fields): FormGroup {
    const formGroup = new FormGroup({});

    Object.keys(fields).map(field => {
      if (typeof fields[field] === 'object') {
        formGroup.addControl(field, this.createFormGroup(fields[field]));
      } else {
        formGroup.addControl(field, new FormControl(fields[field]));
      }
    });
    return formGroup;
  }
}
