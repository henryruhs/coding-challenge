import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { InvoiceInterface, InvoiceStoreInterface } from './invoice.interface';

@Injectable({
  providedIn: 'root'
})
export class InvoiceStore {
  protected store: BehaviorSubject<InvoiceStoreInterface>;

  constructor() {
    this.init([]);
  }

  init(invoices: InvoiceInterface[]): void {
    this.store = new BehaviorSubject<InvoiceStoreInterface>({current: 0, invoices});
  }

  getInstance(): BehaviorSubject<InvoiceStoreInterface> {
    return this.store;
  }

  getStoreValue(): InvoiceStoreInterface {
    return this.store.value;
  }

  getInvoiceByIndex(index: number): InvoiceInterface {
    const invoices: InvoiceInterface[] = this.getStoreValue().invoices;

    return index in invoices ? invoices[index] : null;
  }

  setStoreValue(store: InvoiceStoreInterface): void {
    this.store.next(store);
  }

  setInvoiceByIndex(index: number, invoice: InvoiceInterface): void {
    const invoices: InvoiceInterface[] = this.getStoreValue().invoices;

    if (index in invoices) {
      invoices[index] = invoice;
      this.setStoreValue({
        current: index,
        invoices
      });
    }
  }

  getCurrent(): number {
    return this.getStoreValue().current;
  }

  getNext(): number {
    const current: number = this.getCurrent();

    return current ? current + 1 : 0;
  }

  setCurrent(current: number): void {
    this.setStoreValue({
      current,
      invoices: this.getStoreValue().invoices
    });
  }

  addInvoice(): void {
    this.setStoreValue({
      current: this.getNext(),
      invoices: [
        ...this.getStoreValue().invoices,
        ...[
          {
            customer_id: 0,
            customer_name: 'default',
            customer_contact_person: '',
            customer_address: '',
            customer_zip: '',
            customer_city: '',
            iban: '',
            bic: '',
            account_owner: '',
            mandate_reference: '',
            mandate_city: '',
            mandate_date: '',
            mandate_signee: '',
            invoice_number: '',
            invoice_period: '',
            invoice_date: '',
            invoice_due_date: '',
            line_items: [
              {
                name: '',
                description: '',
                quantity: 1,
                price_cents: 100
              }
            ]
          }
        ]
      ]
    });
  }

  removeInvoiceByIndex(index: number): void {
    this.setStoreValue({
      current: 0,
      invoices:  this.getStoreValue().invoices.filter((value, INDEX) => index !== INDEX)
    });
  }
}

