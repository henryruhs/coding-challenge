import {InvoiceInterface, LineItemInterface} from './invoice.interface';

export function calcItemPrice(lineItems: LineItemInterface[] = []): number {
  let total = 0;

  Object.keys(lineItems).map(value => {
    total += lineItems[value].price_cents * lineItems[value].quantity;
  });
  return total;
}

export function calcInvoicePrice(invoices: InvoiceInterface[]): number {
  let total = 0;

  invoices.map(invoice => {
    total += calcItemPrice(invoice.line_items);
  });
  return total;
}
