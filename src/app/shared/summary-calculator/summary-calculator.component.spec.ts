import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SummaryCalculatorComponent } from './summary-calculator.component';

describe('SummaryCalculatorComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SummaryCalculatorComponent
      ],
      imports: [
        TranslateModule.forRoot()
      ]
    }).compileComponents();
  });

  it('should create the component', () => {
    const fixture = TestBed.createComponent(SummaryCalculatorComponent);
    const component = fixture.componentInstance;

    expect(component).toBeTruthy();
  });
});
