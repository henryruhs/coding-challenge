import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-summary-calculator',
  templateUrl: './summary-calculator.component.html',
  styleUrls: ['./summary-calculator.component.scss']
})
export class SummaryCalculatorComponent {
  @Input() nettoCents: number;
  @Input() valueAddedTax: number;
}
