# Coding Challenge

## Installation

```
npm install
```

## Usage

```
npm start
```

## Todos

1. Fork this repository and commit frequently, so we can track your changes
2. Migrate TSLint to ESLint using `@typescript-eslint` - make sure that `ng lint` is still working
3. Set the application's language according to the browser's `navigator.language`
4. Integrate the bottom `item-calculator` similar to the screen

![item-calculator](.todo/item-calculator.png)
